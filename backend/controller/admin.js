const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();

const { db } = require('../config/db_config');

const mw = require('./middleware.js');

const saltRounds = 12;

router.post('/authenticate', mw.authAdmin, function(req, res) {
    var getCreds = 'SELECT password FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
    db.query(getCreds, ['root'], function (err, result){
        if(err) console.log(err);
        if (result.rows.length > 0) {
            bcrypt.compare(req.body.rootpw, result.rows[0].password, function (err, verdict) {
                if (verdict == true ) {
                    res.status(200).send("Success");
                } else {
                    res.status(202).send('Failure');
                }   
            })  
        }
        else {
            res.status(202).send('No root user');
        }    
    });
})


router.post('/root', mw.authAdmin, function(req, res){
    var getCreds = 'SELECT password FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
    db.query(getCreds, ['root'], function (err, result){
        if(err) console.log(err);
        if (result.rows.length > 0) {
            bcrypt.compare(req.body.oldPassword, result.rows[0].password, function (err, verdict) {
                if (verdict == true ) {
                    bcrypt.hash(req.body.newPassword, saltRounds, function (err,   hash) {
                        var createUser = 'INSERT INTO users (username, password) VALUES ($1, $2) ON CONFLICT (username) DO UPDATE SET password=$2;'; // using placeholders to avoid sql injection
                        db.query(createUser, ['root', hash], function (err){
                            if(err) console.log(err);
                            res.status(200).send("Successful");
                        });
                    })
                } else {
                    res.status(202).send('Wrong old password');
                }   
            })  
        }
        else {
            res.status(202).send('No root user');
        }    
    });
});

// List all users
router.post('/users', mw.authAdmin, function(req, res){
    var getUsers = 'SELECT username FROM users WHERE NOT (username = $1) ORDER BY username;'; // using placeholders to avoid sql injection
        db.query(getUsers, ['root'], function (err, result){
            if(err) console.log(err);
            res.status(200).json(result.rows);
        });
});

router.post('/user', mw.authAdmin, function(req, res){
    if (req.body.username.toLowerCase() === 'root') {
        res.status(202).send('Cannot update root');
    }
    else {
        var getCreds = 'SELECT password FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
        db.query(getCreds, ['root'], function (err, result){
            if(err) console.log(err);
            if (result.rows.length > 0) {
                bcrypt.compare(req.body.rootpw, result.rows[0].password, function (err, verdict) {
                    if (verdict == true ) {
                        bcrypt.hash(req.body.password, saltRounds, function (err,   hash) {
                            var createUser = 'INSERT INTO users (username, password) VALUES ($1, $2) ON CONFLICT (username) DO UPDATE SET password=$2;'; // using placeholders to avoid sql injection
                            db.query(createUser, [req.body.username.toLowerCase(), hash], function (err){
                                if(err) console.log(err);
                                res.status(200).send("Successful");
                            });
                        })
                    } else {
                        res.status(202).send('Wrong old password');
                    }   
                })  
            }
            else {
                res.status(202).send('No root user');
            }    
        });
    }
});
// Delete users
router.post('/deleteUser', mw.authAdmin, function(req, res){
    if (req.body.username.toLowerCase() === 'root') {
        res.status(202).send('Cannot delete root');
    }
    else {
        var getCreds = 'SELECT password FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
        db.query(getCreds, ['root'], function (err, result){
            if(err) console.log(err);
            if (result.rows.length > 0) {
                bcrypt.compare(req.body.rootpw, result.rows[0].password, function (err, verdict) {
                    if (verdict == true ) {
                        var deleteUser = 'DELETE FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
                        db.query(deleteUser, [req.body.username.toLowerCase()], function (err){
                            if(err) console.log(err);
                            res.status(200).send("Success");
                        });
                    } else {
                        res.status(202).send('Wrong root password');
                    }   
                })  
            }
            else {
                res.status(202).send('No root user');
            }    
        });
    }
});


module.exports = router;
