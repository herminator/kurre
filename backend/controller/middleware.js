function authAdmin(req, res, next) {
    if (!req.session.user) {
        res.send('Not logged in');
    }
    else if (req.session.user.toLowerCase() !== 'root') {
        res.send('Not root');
    } else {
        next();
    }
}

function auth(req, res, next) {
    if (!req.session.user) {
        res.send('Not logged in');
    } 
    else if (req.session.user === 'gäst') {
        res.send('Only guest');
    }
    else {
        next();
    }
}

module.exports.authAdmin = authAdmin; 
module.exports.auth = auth;