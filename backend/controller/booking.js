const express = require('express');
const router = express.Router();

const { db } = require('../config/db_config')

const mw = require('./middleware.js');

router.post('/getBookings', mw.auth, function(req, res){
    var getBooking = 'SELECT username, date::varchar(255) FROM bookings WHERE date >= $1 AND date <= $2'; // using placeholders to avoid sql injection
    db.query(getBooking, [req.body.startDate, req.body.endDate], function (err, result){
        if(err) console.log(err);
        res.status(200).json(result.rows);
    });
});

router.post('/book', mw.auth, function(req, res){
    var userList = [];
    req.body.date.forEach(function(date) {
        const arr = date.split('-');
        if (new Date(arr[0], arr[1]-1, arr[2]) < new Date(new Date().toDateString())) {
            res.status(201).send('Past date');
        }
        userList.push(req.session.user);
    });
    var placeBooking = 'INSERT INTO bookings SELECT * FROM UNNEST ($1::text[], $2::date[])'; // using placeholders to avoid sql injection
    db.query(placeBooking, [userList, req.body.date], function (err){
        if(err) console.log(err);
        res.status(200).send('Success');
    });
});

router.post('/delete', mw.auth, function(req, res) {
    req.body.date.forEach(function(date) {
        const arr = date.split('-');
        if (new Date(arr[0], arr[1]-1, arr[2]) < new Date(new Date().toDateString())) {
            res.status(201).send('Past date');
        }
    });
    if (req.session.user === 'root' || req.session.user === 'pappa') {
        var forceDelete = 'DELETE FROM bookings WHERE date IN (SELECT * FROM UNNEST ($1::date[]))'; // using placeholders to avoid sql injection
        db.query(forceDelete, [req.body.date], function (err){
            if(err) console.log(err);
            res.status(200).send('Success');
        });
    }
    else {    
        var deleteBooking = 'DELETE FROM bookings WHERE username=$1 AND date IN (SELECT * FROM UNNEST ($2::date[]))'; // using placeholders to avoid sql injection
        db.query(deleteBooking, [req.session.user, req.body.date], function (err){
            if(err) console.log(err);
            res.status(200).send('Success');
        });
    }
})

module.exports = router;
