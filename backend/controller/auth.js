const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();

const { db } = require('../config/db_config')

const mw = require('./middleware.js');

const saltRounds = 12;

router.post('/login', function(req, res){
    var getCreds = 'SELECT username, password FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
    db.query(getCreds, [req.body.username.toLowerCase()], function (err, result){
        if(err) console.log(err);
        if (result.rows.length > 0) {
            bcrypt.compare(req.body.password, result.rows[0].password, function (err, verdict) {
                if (verdict == true ) {
                    req.session.user = result.rows[0].username;
                    res.status(200).send("Successful");
                } else {
                    res.status(202).send('Wrong credentials');
                }   
            })  
        }  
        else {
            res.status(202).send('No such user');
        }  
    });
});

router.post('/logout', function(req,res){
    if (req.session.user) {
        req.session.destroy(function(){
            res.send("Logged out");
        });
    }
    else {
        res.send("Not even logged in");
    }
});

router.post('/session', function(req,res){
    if (req.session.user) {
        res.status(200).send(req.session.user);
        res.end();
    }
    else {
        res.end();
    }
});

router.put('/update', mw.auth, function(req,res){
    var getCreds = 'SELECT password FROM users WHERE username = $1;'; // using placeholders to avoid sql injection
    db.query(getCreds, [req.session.user], function (err, result){
        if(err) console.log(err);
        if (result.rows.length > 0) {
            bcrypt.compare(req.body.oldPassword, result.rows[0].password, function (err, verdict) {
                if (verdict == true ) {
                    bcrypt.hash(req.body.newPassword, saltRounds, function (err,   hash) {
                        var updatePassword = 'UPDATE users SET password=$2 WHERE username=$1;'; // using placeholders to avoid sql injection
                        db.query(updatePassword, [req.session.user, hash], function (err){
                            if(err) console.log(err);
                            res.status(200).send('Success');
                        });
                    })
                } else {
                    res.status(202).send('Wrong password');
                }   
            })  
        }
        else {
            res.status(202).send('No such user');
        }    
    });
});

module.exports = router;
