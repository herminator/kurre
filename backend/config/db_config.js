require('dotenv').config()

const { Pool } = require('pg')

const isProduction = process.env.NODE_ENV === 'production';

const connectionString = 'postgres://ytpnpuyqzgaryl:709a1094ebea0f78c1563e271e634d5f16bacdbe806b09607c0f9f1391810f1d@ec2-54-75-231-215.eu-west-1.compute.amazonaws.com:5432/d7d85d8n6d66ju';

const db = new Pool({
  connectionString: isProduction ? process.env.DATABASE_URL : connectionString,
  ssl: {
    rejectUnauthorized: false
  }
});

module.exports = { db }