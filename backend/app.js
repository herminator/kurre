var sslRedirect = require('heroku-ssl-redirect');
const express = require('express')
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
const history = require('connect-history-api-fallback');
const session = require('express-session');
var pgSession = require('connect-pg-simple')(session);

const auth = require('./controller/auth.js');
const admin = require('./controller/admin.js');
const booking = require('./controller/booking.js');

const port = process.env.PORT || 5000

const app = express()

// enable ssl redirect
app.use(sslRedirect());

// Parse requests of content-type 'application/json'
app.use(bodyParser.json());

// HTTP request logger
app.use(morgan('dev'));

//app.options('*', cors());
//app.use(cors());
app.use(cors({credentials: true, origin: true}));

// Setting up the pool to the db
const { db } = require('./config/db_config.js')

app.set('trust proxy', 1) // trust first proxy

//Use session ids
app.use(session({
    store: new pgSession({
        pool: db
      }),
    secret: 'dasistgut',
    resave: false,
    saveUninitialized: false,
    cookie: { 
        secure: process.env.NODE_ENV === 'production' ? true : false ,
        maxAge: 24 * 60 * 60 * 1000,
        httpOnly: false
    },
}));

app.use(express.static(path.join(__dirname + '/..', 'frontend/build')));

app.post('/api', (req, res) => res.send('Backend fungerar'))

// Define routes
app.use('/api/auth', auth);
app.use('/api/admin', admin);
app.use('/api/booking', booking);

// The following is there to show a 404 page for every URL not specified
app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname + '/..', 'frontend/build/index.html'));
});

app.use(history());

app.listen(port, () => console.log(
    `Kurre app listening at http://localhost:${port}`)
)