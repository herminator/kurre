import axios from 'axios'

export const Api = axios.create({
  withCredentials: true,
  baseURL: process.env.NODE_ENV === 'production' ? process.env.REACT_APP_API_ENDPOINT : 'http://localhost:5000/api' 
})
