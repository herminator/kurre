import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavigationBar from './components/NavigationBar';

import { Api } from './Api.js';

import Home from './pages/Home';
import Book from './pages/Book';
import Gallery from './pages/Gallery';
import GuestBook from './pages/GuestBook';
import Admin from './pages/Admin';
import UserPage from './pages/UserPage';
import P404 from './pages/404';
import Login from './pages/Login';
import PrivateRoute from './pages/PrivateRoute';
import SuperPrivateRoute from './pages/SuperPrivateRoute';
//import Game from './components/Game.js';

class App extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
      user: null,
      loading: true,
    };
  }

  checkLogin() {
    Api.post('/auth/session')
        .then(response => {
          if (response.data) {
            this.setState({user: response.data});
          }
          else {
            this.setState({user: ''});
          }
        })
        .catch(error => {
          console.log(error)
        })
  }

  setVisible() {
    const site = document.getElementById("site");
    site.classList.remove('invisible');
    
    const body = document.body;
    body.classList.remove('no-scroll');

    const loader = document.getElementById("loader");
    loader.classList.add('invisible');
  }

  // Fetch the list on first mount
  componentDidMount() {
    this.checkLogin();
    setTimeout(
      () => this.setVisible(), 
      800
    );
  }

  /*  Include in app later when fixed
    <Game user={this.state.user} />
  */

  render() {
    const App = () => (
      <>
        <div id="loader" className="ripple"><div></div><div></div></div>
        <div id="overlay"></div>
        <div id="site" className='invisible'>
          <NavigationBar user={this.state.user} />
          <Switch>
            <PrivateRoute exact path='/' component={Home} user={this.state.user}/>
            <PrivateRoute exact path='/boka' component={Book} user={this.state.user}/>
            <PrivateRoute exact path='/galleri' component={Gallery} user={this.state.user}/> 
            <PrivateRoute exact path='/gästbok' component={GuestBook} user={this.state.user}/>
            <PrivateRoute exact path="/användare/:username" component={UserPage} user={this.state.user}/>
            <SuperPrivateRoute exact path='/admin' component={Admin} user={this.state.user} denial={P404}/>
            <Route exact path="/loggain" render={(props) => (
              !this.state.user ? 
                <Login {...props} user={this.state.user} /> : <Redirect to={'/'} />   
              )}
            />
            <PrivateRoute component={P404} user={this.state.user}/>
          </Switch>
        </div>
      </>
    )
    return (
      <Switch>
        <App/>
      </Switch>
    );
  }
}

export default App;
