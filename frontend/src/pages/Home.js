import React, { Component } from 'react';
import { Container, Carousel, Image, Row, Col } from 'react-bootstrap';

class Home extends Component {

  constructor(props){
    super(props);
    this.state = {
    }
  }

  // On first mount
  componentDidMount() {
  }

  render() {
    return (
      <Container className="App">
        <Carousel touch={true}>
          <Carousel.Item>
            <Image
              className="d-block w-100"
              src={require('../assets/img/kurre.jpg')}
              alt="Bild på Kurre framför Städjan"
            />
          </Carousel.Item>
          <Carousel.Item>
            <Image
              className="d-block w-100"
              src={require('../assets/img/ren.jpg')}
              alt="En ren springer på fjället"
            />
          </Carousel.Item>
          <Carousel.Item>
            <Image
              className="d-block w-100"
              src={require('../assets/img/nip.jpg')}
              alt="The man of Nip"
            />
          </Carousel.Item>
        </Carousel>
        <Row>
          <Col className="col-6 offset-3 col-md-4 offset-md-0 mt-3">
            <a href="/boka"><Image src={require('../assets/img/booking.jpg')} className="container-fluid"/>
              <h2 className="mt-2">Boka</h2>
            </a>
          </Col>
          <Col md className="col-6 offset-3 col-md-4 offset-md-0 mt-3">
            <a href="/galleri"><Image src={require('../assets/img/gallery.jpg')} className="container-fluid"/>
              <h2 className="mt-2">Galleri</h2>
            </a>
          </Col>
          <Col className="col-6 offset-3 col-md-4 offset-md-0 mt-3">
            <a href="/gästbok"><Image src={require('../assets/img/guest_book.jpg')} className="container-fluid"/>
              <h2 className="mt-2">Gästbok</h2>
            </a>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Home;