// This is used to determine if a user is authenticated and
// if they are allowed to visit the page they navigated to.

// If they are: they proceed to the page
// If not: they are redirected to the login page.
import React from 'react'
import { Redirect, Route } from 'react-router-dom'

const PrivateRoute = ({ component: Component, user: User}) => {
  if (User === null) {
    return (<></>)
  }
  else {
    return (
      <Route
        render={() =>
          User ? (
            <Component user={User}/>
          ) : (
            <Redirect to={'/loggain'} />
          )
        }
      />
    )
  }
}

export default PrivateRoute