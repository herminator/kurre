import React, { Component } from 'react';
import { Container, Form, Button } from 'react-bootstrap';

import { Helmet } from 'react-helmet';

import { Api } from '../Api.js';

class UserPage extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
      success: '',
      warning: '',
      oldPassword: '',
      newPassword: '',
      newPasswordRepeated: ''
    }
  }

  validateNewPassword() {
    return this.state.newPassword.length > 0 && this.state.newPasswordRepeated.length > 0 && 
    (this.state.newPassword.localeCompare(this.state.newPasswordRepeated) === 0);
  }

  checkIfEqual() {
    return this.state.newPassword.length > 0 && this.state.newPasswordRepeated.length > 0 && 
    (this.state.newPassword.localeCompare(this.state.newPasswordRepeated) !== 0);
  }

  updatePassword() {
    const userInfo = {
      oldPassword: this.state.oldPassword,
      newPassword: this.state.newPassword,
      newPasswordRepeated: this.state.newPasswordRepeated
    }
    this.setState({success: ''});
    this.setState({warning: ''});
    Api.put('auth/update', userInfo)
      .then(response => {
        if (response.data === 'Success') {
          this.setState({success: 'Lösenord uppdaterat'})
          this.setState({oldPassword: ''})
          this.setState({newPassword: ''})
          this.setState({newPasswordRepeated: ''})
        } else if (response.data === 'Wrong password'){
          this.setState({warning: 'Fel gammalt lösenord'})
          this.setState({oldPassword: ''})
          this.setState({newPassword: ''})
          this.setState({newPasswordRepeated: ''})
        }else {
          this.setState({warning: 'Något gick fel'})
          this.setState({oldPassword: ''})
          this.setState({newPassword: ''})
          this.setState({newPasswordRepeated: ''})
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  // Fetch the list on first mount
  componentDidMount() {
  }

  render() {
    return (
      <>
        <Helmet>
          <title>{this.props.user}</title>
        </Helmet>
        <Container className="App col-md-6">
        <h1>Ändra lösenord</h1>
            {this.state.success && <p className="text-success text-left">{ this.state.success }</p>}
            {this.state.warning && <p className="text-danger text-left">{ this.state.warning }</p>}
            <Form className="text-left" onSubmit={e => {e.preventDefault(); this.updatePassword()}}>
              <Form.Group controlId="password_user_old">
                <Form.Label>Gammalt lösenord</Form.Label>
                <Form.Control
                    type="password"
                    value={this.state.oldPassword}
                    onChange={e => this.setState({oldPassword: e.target.value})}
                    placeholder="Skriv in ditt gamla lösenord" 
                />
              </Form.Group>
              <Form.Group controlId="password_user_new">
                <Form.Label>Nytt lösenord</Form.Label>
                <Form.Control
                    value={this.state.newPassword}
                    onChange={e => this.setState({newPassword: e.target.value})}
                    type="password"
                    placeholder="Skriv in ditt nya lösenord" 
                />
              </Form.Group>
              <Form.Group controlId="password_user_new">
                <Form.Label>Upprepa nytt lösenord 
                  {this.checkIfEqual() && <span className="text-danger ml-3">Lösenord stämmer inte överens</span>}
                  {this.validateNewPassword() && <span className="text-success ml-3">Lösenord matchar</span>}
                  </Form.Label>
                <Form.Control
                    value={this.state.newPasswordRepeated}
                    onChange={e => this.setState({newPasswordRepeated: e.target.value})}
                    type="password"
                    placeholder="Upprepa ditt nya lösenord" 
                />
              </Form.Group>
              <Button className="" variant="primary" disabled={!this.validateNewPassword()} type="submit">Uppdatera lösenord</Button>
            </Form>
        </Container>
      </>
    );
  }
}

export default UserPage;