import React, { Component } from 'react';
import { Container } from 'react-bootstrap';

import { Helmet } from 'react-helmet'

class GuestBook extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
    }
  }

  // Fetch the list on first mount
  componentDidMount() {
  }

  render() {
    return (
      <>
        <Helmet>
          <title>Gästbok</title>
        </Helmet>
        <Container className="App">
          <h1>Detta kommer att vara gästbokssidan.</h1>
        </Container>
      </>
    );
  }
}

export default GuestBook;