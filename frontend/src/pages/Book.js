import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import { Helmet } from 'react-helmet';

import Calendar from '../components/Calendar'

class Book extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
    }
  }

  // Fetch the list on first mount
  componentDidMount() {

  }

  render() {
    return (
      <>
        <Helmet>
          <title>Boka</title>
        </Helmet>
        <Container className="App">
          <Calendar user={this.props.user}/>
        </Container>
      </>
    );
  }
}

export default Book;