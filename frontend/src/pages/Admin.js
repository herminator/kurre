import React, { Component } from 'react';
import { Container, Form, Button, Modal, Table } from 'react-bootstrap';
import { Helmet } from 'react-helmet'

import { Api } from '../Api.js';

import { faSkullCrossbones } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Admin extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
      users: [],
      username: '',
      password: '',
      success: '',
      warning: '',
      show: false,
      message:'',
      rootpw: '',
      rootSuccess: '',
      rootWarning: '',
      rootOld: '',
      rootNew: '',
      rootRepeated: '',
      userDelete: '',
      modalMsg: ''
    };
  }

  // Dynamic function for authentication
  dynamicFunction = () => {};

  setDynamicFunction(newFunc) {
    this.dynamicFunction = newFunc;
  }

  getUsers() {
    Api.post('admin/users')
        .then(response => {
          this.setState({users: response.data})
        })
        .catch(error => {
          console.log(error)
        })
  }

  capitalise(s) {
    if (s !== null) {
      return s.charAt(0).toUpperCase() + s.slice(1)
    }
  }

  validateForm() {
    return this.state.username.length > 0 && this.state.password.length > 0;
  }

  validateRootPassword() {
    return this.state.rootNew.length > 0 && this.state.rootRepeated.length > 0 && (this.state.rootNew.localeCompare(this.state.rootRepeated) === 0);
  }

  createUser() {
    const userInfo = {
      username: this.state.username,
      password: this.state.password,
      rootpw: this.state.rootpw
    }
    this.setState({success: ''});
    this.setState({warning: ''});
    Api.post('admin/user', userInfo)
      .then(response => {
        if (response.data === 'Successful') {
          this.setState({success: 'Konto skapat'})
          this.getUsers();
          this.setState({username: ''})
          this.setState({password: ''})
        } else {
          this.setState({warning: 'Något gick fel'})
          this.setState({username: ''})
          this.setState({password: ''})
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  deleteUser() {
    const userInfo = {
      username: this.state.userDelete,
      rootpw: this.state.rootpw
    }
    this.setState({success: ''});
    this.setState({warning: ''});
    Api.post('admin/deleteUser', userInfo)
      .then(response => {
        if (response.data === 'Success') {
          this.close();
          this.getUsers();
          this.setState({username: ''})
        } else {
          this.close();
          this.setState({username: ''})
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  updateRoot() {
    const rootInfo = {
      oldPassword: this.state.rootOld,
      newPassword: this.state.rootNew
    }
    this.setState({rootSuccess: ''});
    this.setState({rootWarning: ''});
    Api.post('admin/root', rootInfo)
      .then(response => {
        if (response.data === 'Successful') {
          this.setState({rootSuccess: 'Lösenord uppdaterat'})
          this.setState({rootOld: ''})
          this.setState({rootNew: ''})
          this.setState({rootRepeated: ''})
        } else {
          this.setState({rootWarning: 'Något gick fel'})
          this.setState({rootOld: ''})
          this.setState({rootNew: ''})
          this.setState({rootRepeated: ''})
        }
      })
      .catch(error => {
        console.log(error)
      })
  }

  authRoot() {
    const credential = {
      rootpw: this.state.rootpw
    }
    Api.post('admin/authenticate', credential)
      .then(response => {
        if (response.data === 'Success') {
          this.dynamicFunction();
          this.close();
        } else {
          this.setState({message: 'Fel lösenord'})
          this.setState({rootpw: ''})
        }
      })
      .catch(error => {
        console.log(error)
      })
  }


  close() {
    this.setState({show: false})
    this.setState({message: ''})
    this.setState({rootpw: ''})
  }

  // Fetch the list on first mount
  componentDidMount() {
    this.getUsers();
  }

  render() {
    const tmp = this.state.users;
    const users = tmp.map((n) =>
      <tr key={n.username}>
        <td className='text-left'><span className="userList">{this.capitalise(n.username)}</span>
          <Button variant="outline-secondary" className="kill-button float-right" onClick={() => {this.setDynamicFunction(this.deleteUser); this.setState({userDelete: n.username, show: true, modalMsg: ` - Ta bort ${this.capitalise(n.username)}`})}} >
            <FontAwesomeIcon icon={faSkullCrossbones} size="lg" className="ml-2 mr-2"/>
          </Button>
        </td>
      </tr>
    );

    return (
      <>
        <Helmet>
          <title>Admin</title>
        </Helmet>

        {this.props.user !== 'root' && 
          <Container className="App">
            <img
              className="hidden-lg w-100"
              src={require('../assets/img/unauthorised.jpg')}
              alt="Du har inte behörighet att se denna sida."
            />   
          </Container>
        }

        {this.props.user === 'root' && 
          <Container className="App col-md-6">
            <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                  <th>Användarnamn</th>
                </tr>
              </thead>
              <tbody>
                { users }
              </tbody>
            </Table>

            <div className="border border-secondary rounded mt-4 p-2">
            <h1 className="">Skapa konto</h1>
            {this.state.success && <p className="text-success text-left">{ this.state.success }</p>}
            {this.state.warning && <p className="text-danger text-left">{ this.state.warning }</p>}
            <Form className="text-left" onSubmit={e => {e.preventDefault(); this.setDynamicFunction(this.createUser); this.setState({show: true, modalMsg: ` - Lägg till ${this.capitalise(this.state.username)}`})}}>
              <Form.Group controlId="username_user">
                <Form.Label>Användarnamn</Form.Label>
                <Form.Control
                    type="text"
                    value={this.state.username}
                    onChange={e => this.setState({username: e.target.value})}
                    placeholder="Skriv in användarnamn" 
                />
              </Form.Group>

              <Form.Group controlId="password_user">
                <Form.Label>Lösenord</Form.Label>
                <Form.Control
                    value={this.state.password}
                    onChange={e => this.setState({password: e.target.value})}
                    type="password"
                    placeholder="Skriv in lösenord" 
                />
              </Form.Group>
              <Button className="" variant="primary" disabled={!this.validateForm()} type="submit">Skapa konto</Button>
            </Form>
            </div>

            <div className="border border-secondary rounded mt-4 p-2 mb-4">
            <h1 className="">Ändra admin-lösenord</h1>
            {this.state.rootSuccess && <p className="text-success text-left">{ this.state.rootSuccess }</p>}
            {this.state.rootWarning && <p className="text-danger text-left">{ this.state.rootWarning }</p>}
            <Form className="text-left" onSubmit={e => {e.preventDefault(); this.updateRoot()}}>
              <Form.Group controlId="password_old">
                <Form.Label>Gammalt lösenord</Form.Label>
                <Form.Control
                    value={this.state.rootOld}
                    onChange={e => this.setState({rootOld: e.target.value})}
                    type="password"
                    placeholder="Skriv in gammalt lösenord" 
                />
              </Form.Group>
              <Form.Group controlId="password_new">
                <Form.Label>Nytt lösenord</Form.Label>
                <Form.Control
                    value={this.state.rootNew}
                    onChange={e => this.setState({rootNew: e.target.value})}
                    type="password"
                    placeholder="Skriv in nytt lösenord" 
                />
              </Form.Group>
              <Form.Group controlId="password_new_repeated">
                <Form.Label>Upprepa nytt lösenord</Form.Label>
                <Form.Control
                    value={this.state.rootRepeated}
                    onChange={e => this.setState({rootRepeated: e.target.value})}
                    type="password"
                    placeholder="Upprepa nytt lösenord" 
                />
              </Form.Group>
              <Button className="" variant="primary" disabled={!this.validateRootPassword()} type="submit">Ändra</Button>
            </Form>
            </div>

            <Modal show={this.state.show} onHide={() => {this.close()}} animation={false}>
              <Modal.Header closeButton>
      <Modal.Title>Skriv in lösenord till admin { this.state.modalMsg} </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {this.state.message && <p className="text-danger">{ this.state.message }</p>}
                <Form onSubmit={e => {e.preventDefault(); this.authRoot()}}>
                  <Form.Group controlId="confirmPassword">
                      <Form.Label>Lösenord</Form.Label>
                      <Form.Control
                          autoFocus
                          value={this.state.rootpw}
                          onChange={e => this.setState({rootpw: e.target.value})}
                          type="password"
                      />
                  </Form.Group>
                  <Button className="float-right" variant="primary" type="submit">Godkänn</Button>
                </Form>
              </Modal.Body>
            </Modal>
          </Container>
        }
      </>
    );
  }
}

export default Admin;