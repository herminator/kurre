// This is used to determine if a user is authenticated and
// if they are allowed to visit the page they navigated to.

// If they are: they proceed to the page
// If not: they are redirected to the login page.
import React from 'react'
import { Route } from 'react-router-dom'

const SuperPrivateRoute = ({ component: Component, user: User, denial: P404}) => {

  return (
    <Route
      render={() =>
        (User === 'root') ? (
          <Component user={User}/>
        ) : (
          <P404/>
        )
      }
    />
  )
}

export default SuperPrivateRoute