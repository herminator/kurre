import React, { Component } from 'react';
import { Container } from 'react-bootstrap';

import { Helmet } from 'react-helmet'

class NotFound extends Component {
  // Initialize the state
  constructor(props){
    super(props);
    this.state = {
    }
  }

  // Fetch the list on first mount
  componentDidMount() {
  }

  render() {
    return (
      <>
        <Helmet>
          <title>404</title>
        </Helmet>
        <Container className="App">
          <img
            className="w-100"
            src={require('../assets/img/404.png')}
            alt="Sidan finns inte"
          />      
        </Container>
      </>
    );
  }
}

export default NotFound;