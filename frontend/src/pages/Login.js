import React, { Component } from 'react';
import { Container, Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';

import { Api } from '../Api.js';

import { Helmet } from 'react-helmet'

class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
          username: '',
          password: '',
          message: ''
        };
      }
    
    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    login() {
        const loginInfo = {
            username: this.state.username,
            password: this.state.password
        }
        Api.post('auth/login', loginInfo)
            .then(response => {
            if (response.data === 'Successful') {
                //window.location.reload(false);          
                window.location.assign('/');

            } else {
                this.setState({message: 'Felaktiga uppgifter'})
                this.setState({username: ''})
                this.setState({password: ''})
            }
            })
            .catch(error => {
            console.log(error)
            })
    }

  // Fetch the list on first mount
  componentDidMount() {
  }

  render() {
    return (
      <>
        <Helmet>
          <title>Login</title>
        </Helmet>
        <Container className="App col-sm-6 col-md-4 text-left">
            {this.state.message && <p className="text-danger">{ this.state.message }</p>}
            <Form onSubmit={e => {e.preventDefault(); this.login()}}>
                <FormGroup controlId="kurre_username">
                    <FormLabel>Användarnamn</FormLabel>
                    <FormControl
                        autoFocus
                        type="text"
                        value={this.state.username}
                        onChange={e => this.setState({username: e.target.value})}
                    />
                </FormGroup>
                <FormGroup controlId="kurre_password">
                    <FormLabel>Lösenord</FormLabel>
                    <FormControl
                        value={this.state.password}
                        onChange={e => this.setState({password: e.target.value})}
                        type="password"
                    />
                </FormGroup>
                <Button variant="primary" disabled={!this.validateForm()} type="submit">Logga in</Button>
            </Form>     
        </Container>
      </>
    );
  }
}

export default Login;