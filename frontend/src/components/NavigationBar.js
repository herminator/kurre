import React, { Component } from 'react';
import { Nav, Navbar, Button} from 'react-bootstrap';

import { Api } from '../Api.js';

export default class NavigationBar extends Component {

  logout() {
    Api.post('auth/logout')
      .then(response => {
          window.location.assign('/');
      })
      .catch(error => {
        console.log(error)
      })
  }

  startBotherMom() {
    var d = document.getElementById('botherMom');
    d.classList.add('spinner');
    setTimeout(function() { 
      this.repeatBotherMom();
    }.bind(this), 1200);
  }

  repeatBotherMom() {
    var d = document.getElementById('botherMom');
    d.classList.remove('spinner');
    setTimeout(function() { 
      this.startBotherMom();
    }.bind(this), Math.random()*5*60*1000);
  }

  componentDidMount() {
    if (this.props.user === 'mamma') {
      setTimeout(function() { 
        this.startBotherMom();
      }.bind(this), Math.random()*5*60*1000);
    }
  }

	render() {
		return (
      <div>
        <Navbar bg="light" expand="lg" className="mb-2">
        <Navbar.Brand href="/">
          <div id="botherMom" className="navName ml-2 mr-2">
            <img src={require('../assets/img/nav.png')} className="mr-2 mt-2 mb-2" alt="Bild på en husbil"></img>
            Kurre
          </div>
        </Navbar.Brand>
        { this.props.user &&
          <>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" show={this.props.user}>
              <Nav className="mr-auto">
                <Nav.Link href="/boka">Boka</Nav.Link>
                <Nav.Link href="/galleri">Galleri</Nav.Link>
                <Nav.Link href="/gästbok">Gästbok</Nav.Link>
              </Nav>
              <Nav className="ml-auto mr-3 mt-lg-2 mb-2">
                <Nav.Link href={ this.props.user === 'root' ? '/admin' : `/användare/${this.props.user}` } style={{ color: 'green' }}>{this.props.user}</Nav.Link>
              </Nav>
              <Button variant="outline-secondary" onClick={() => {this.logout()}}>
                Logga ut
              </Button>
            </Navbar.Collapse>
          </>
        }
        </Navbar>
      </div>
		);
	}
}