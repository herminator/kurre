import React, { Component } from 'react';
import { Row, Col, Button, Modal } from 'react-bootstrap';

import { Api } from '../Api.js';

import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Calendar extends Component {

  constructor(props){
    super(props);
    this.state = {
        isAuth: null,
        todaysDate: new Date(),
        currentMonth: new Date(),
        currentMonthDays: [],
        currentWeeks: [],
        monthsNames: [
            "Januari", "Februari", "Mars", "April", "Maj", "Juni",
            "Juli", "Augusti", "September", "Oktober", "November", "December"
          ],
        days: [],
        toggle: 'standard',
        bookings: null,
        clickedDayOrig: '',
        clickedDayUser: '',
        clickedDates: [],
        show: false,
        success: '',
        warning: ''
    };
  }

  isAuth() {
    if (this.props.user === 'root' || this.props.user === 'pappa'){
      this.setState({isAuth: true});
    }
    else {
      this.setState({isAuth: false});
    }
  }

  buildCurrentMonthDays = () => {      
    var curYear = this.state.currentMonth.getFullYear(),
        curMonth = this.state.currentMonth.getMonth(),
        firstMonthDay = new Date(curYear, curMonth, 1),
        lastMonthDay = new Date(curYear, curMonth + 1, 0),
        tmp = [];

    // push visible previous month days
    for (let i = -firstMonthDay.getUTCDay(); i < 0; i++) {
        tmp = tmp.concat([new Date(curYear, curMonth , i + 1)]);
    }

    // push current month days
    for (let i = 1, lastDay = lastMonthDay.getDate(); i <= lastDay; i++) {
        tmp = tmp.concat([new Date(curYear, curMonth, i)]);
    }

    // push visible next month days
    for (let i = 1, daysAppend = 7 - lastMonthDay.getUTCDay(); i < daysAppend; i++) {
        tmp = tmp.concat([new Date(curYear, curMonth + 1, i)]);
    }

    return tmp;
  }

  getBookings() {
    const info = {
      startDate: this.getDBDate(this.state.currentMonthDays[0]),
      endDate: this.getDBDate(this.state.currentMonthDays[this.state.currentMonthDays.length - 1])
    }
    Api.post('booking/getBookings', info)
        .then(response => {
          this.setState({bookings: response.data}, () => {this.generateDays(); this.generateWeeks()})
        })
        .catch(error => {
          console.log(error)
        })
  }

  getDayClass(date) {
    var classes = ['noselect calendar-days'];

    classes = classes.concat(this.adaptCalendar(date));

    // if date is selectedDate
    if (date.toDateString() === this.state.todaysDate.toDateString()) {
      // add default and custom active classes
      classes = classes.concat(['calendar-days--active']);
    }

    // if date is from previous year
    if (date.getMonth() === 11 && this.state.currentMonth.getMonth() === 0) {
      // mark as previous month date
      classes.push('calendar-days--prev-month');
    // if date is from next year
    } else if (date.getMonth() === 0 && this.state.currentMonth.getMonth() === 11) {
      // mark as next month date
      classes.push('calendar-days--next-month');
    // if date is from previous month
    } else if (date.getMonth() < this.state.currentMonth.getMonth()) {
      classes.push('calendar-days--prev-month');
    // if date is from next month
    } else if(date.getMonth() > this.state.currentMonth.getMonth()) {
      classes.push('calendar-days--next-month');
    }

    // return element class string
    return classes.join(' ');
  }

  checkIfBooked(date) {
    const fdate = this.getDBDate(date);
    var verdict = false;

    if (this.props.user !== 'gäst') {
      this.state.bookings.forEach(function(booking) {
        if (booking.date === fdate) {
          verdict = true;
        }
      });
    }

    return verdict;
  }

  checkWhoBooked(date) {
    const fdate = this.getDBDate(date);
    var user = null;

    if (this.props.user !== 'gäst') {
      this.state.bookings.forEach(function(booking) {
        if (booking.date === fdate) {
          user = booking.username;
        }
      });
    }

    return user;
  }

  checkIfBookedByMe(date) {
    const fdate = this.getDBDate(date);
    var verdict = false;
    const user = this.props.user;

    if (this.props.user !== 'gäst') {
      this.state.bookings.forEach(function(booking) {
        if (booking.date === fdate && booking.username === user) {
          verdict = true;
        }
      });
    }

    return verdict;
  }

  checkIfClicked(date) {
    const fdate = this.getDBDate(date);
    var verdict = false;
    this.state.clickedDates.forEach(function(click) {
      if (click === fdate) {
        verdict = true;
      }
    });
    return verdict;
  }

  adaptCalendar(date) {
    var classes = [];

    if (this.state.show === false) {
      if (this.state.toggle === 'standard') {
        if (this.checkIfBookedByMe(date)) {
          classes.push('calendar-clickable');
          classes.push('calendar-booked-self');
        }
        else if (this.checkIfBooked(date)) {
          classes.push('calendar-clickable');
          classes.push('calendar-booked');
        }
        else {
          classes.push('calendar-clickable')
        }
        return classes;
      }
      else if (this.state.toggle === 'book') {
        if (this.checkIfBookedByMe(date)) {
          classes.push('calendar-booked-self-unavailable');
        }
        else if (this.checkIfBooked(date)) {
          classes.push('calendar-booked-unavailable');
        }
        else if (date < new Date(new Date().toDateString())) {
          classes.push('calendar-unavailable')
        }
        else {
          classes.push('calendar-clickable')
        }
        if (this.checkIfClicked(date)) {
          classes.push('calendar-clicked');
        }
        return classes;
      }
      else if (this.state.toggle === 'delete') {
        if (this.checkIfBookedByMe(date)) {
          if (date < new Date(new Date().toDateString())) {
            classes.push('calendar-booked-self-unavailable')
          }
          else {
            classes.push('calendar-clickable')
            classes.push('calendar-booked-self');
          }
        }
        else if (this.checkIfBooked(date)) {
          if (date < new Date(new Date().toDateString())) {
            classes.push('calendar-booked-unavailable')
          }
          else {
            if (this.state.isAuth) {
              classes.push('calendar-clickable')
              classes.push('calendar-booked');
            }
            else {
              classes.push('calendar-booked-unavailable');
            }
          }
        }
        else {
          classes.push('calendar-unavailable')
        }
        if (this.checkIfClicked(date)) {
          classes.push('calendar-remove');
        }
        return classes;
      }
    }
    else {
      if (this.state.clickedDates[0] === this.getDBDate(date)) {
        if (this.checkIfBookedByMe(date)) {
          classes.push('calendar-booked-self');
        }
        else if (this.checkIfBooked(date)) {
          classes.push('calendar-booked');
        }
        else {
          classes.push('calendar-clicked')
        }
      }
      else {
        if (this.checkIfBookedByMe(date)) {
          classes.push('calendar-booked-self-unavailable');
        }
        else if (this.checkIfBooked(date)) {
          classes.push('calendar-booked-unavailable');
        }
        else {
          classes.push('calendar-unavailable')
        }
      }
      return classes;
    }
  }

  generateDays() {
    var day_list = [];

    this.state.currentMonthDays.forEach(function(day) {
      day_list.push([`${day}`, `${this.getDayClass(day)}`, `${day.getDate()}`]);
    }.bind(this));

    this.setState({days: day_list});
  }

  generateWeeks() {
    var week_list = [];

    for (var i=0 ; i < this.state.currentMonthDays.length ; i = i+7) {
      week_list.push(this.getWeekNumber(this.state.currentMonthDays[i]));
    }

    this.setState({currentWeeks: week_list});
  }

  generateMonth() {
    this.setState({currentMonthDays: this.buildCurrentMonthDays()}, () => {
      this.getBookings();
    });
  }

  // Get the date formatted to fit with database
  getDBDate(date) {
    var month = '' + (date.getMonth() + 1);
    var day = '' + date.getDate();
    var year = date.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }

  // Get the date formatted for display
  getFormattedDate(date) {
    return `${date.getFullYear()} ${this.state.monthsNames[date.getMonth()]}`;
  }

  getWeekNumber(date) {
    // Copy date so don't modify original
    date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay()||7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(date.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (date - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return weekNo;
  }

  prevMonth() {
    this.showLoading();
    var curYear = this.state.currentMonth.getFullYear(),
        curMonth = this.state.currentMonth.getMonth();
    // set currentMonth to month before
    this.setState({currentMonth: new Date(curYear, curMonth - 1, 1), success: '', warning: ''}, () => {this.hideLoading(); this.generateMonth()});
  }

  nextMonth() {
    this.showLoading();
    var curYear = this.state.currentMonth.getFullYear(),
        curMonth = this.state.currentMonth.getMonth();
    // set currentMonth to month after
    this.setState({currentMonth: new Date(curYear, curMonth + 1, 1), success: '', warning: ''}, () => {this.hideLoading(); this.generateMonth()});
  }

  enableBookingMode() {
    this.setState({toggle: 'book'}, () => {this.generateDays()})
  }

  confirmBooking() {
    this.showLoading();
    var info = null;
    if (this.state.clickedDates.length === 0) {
      this.setState({toggle: 'standard'}, () => {this.hideLoading(); this.generateMonth()});
      return;
    }
    else if (this.state.clickedDates.length > 0) {
      info = {
        date: this.state.clickedDates
      }
    }
    this.setState({success: '', warning: ''});
    Api.post('booking/book', info)
        .then(response => {
          if (response.data === 'Success') {
            this.setState({show: false, clickedDates: [], success: 'Lyckat', toggle: 'standard'}, () => {this.hideLoading(); this.generateMonth()})
          } else {
            this.setState({warning: 'Något gick fel', clickedDates: [], toggle: 'standard'}, () => {this.hideLoading(); this.generateMonth()})
          }
        })
        .catch(error => {
          console.log(error)
        })
    setTimeout(
      () => this.setState({success: '', warning: ''}), 
      2000
    );
  }

  disableBookingMode() {
    this.setState({toggle: 'standard', clickedDates: []}, () => {this.generateDays()})
    this.generateDays()
  }

  enableDeletionMode() {
    this.setState({toggle: 'delete'}, () => {this.generateDays()})
  }

  confirmDeletion() {
    this.showLoading();
    var info = null;
    if (this.state.clickedDates.length === 0) {
      this.setState({toggle: 'standard'}, () => {this.hideLoading(); this.generateMonth()});
      return;
    }
    else if (this.state.clickedDates.length > 0) {
      info = {
        date: this.state.clickedDates
      }
    }
    this.setState({success: '', warning: ''});
    Api.post('booking/delete', info)
        .then(response => {
          if (response.data === 'Success') {
            this.setState({show: false, clickedDates: [], success: 'Lyckat', toggle: 'standard'}, () => {this.hideLoading(); this.generateMonth()})
          } else {
            this.setState({warning: 'Något gick fel', clickedDates: [], toggle: 'standard'}, () => {this.hideLoading(); this.generateMonth()})
          }
        })
        .catch(error => {
          console.log(error)
        })
    setTimeout(
      () => this.setState({success: '', warning: ''}), 
      2000
    );
  }

  disableDeletionMode() {
    this.setState({toggle: 'standard', clickedDates: []}, () => {this.generateDays()})
    this.generateDays()
  }

  showLoading() {
    const loader = document.getElementById("loader");
    loader.classList.remove('invisible');
  }

  hideLoading() {
    const loader = document.getElementById("loader");
    loader.classList.add('invisible');
  }

  clickDate(event) {
    var target = event.target;
    if (this.state.toggle === 'standard') {
      const date = new Date(target.dataset.date)
      if (target.classList.contains('calendar-clickable')) {
        this.setState({
          clickedDates: this.state.clickedDates.concat([this.getDBDate(date)]), 
          clickedDayOrig: date,
          clickedDayUser: this.capitalise(this.checkWhoBooked(date)),
          show: true
        }, () => this.generateDays())
      }
    }
    else if (this.state.toggle === 'book') {
      const date = new Date(target.dataset.date);
      const fdate = this.getDBDate(date);
      if (target.classList.contains('calendar-clicked')) {
        target.classList.remove('calendar-clicked');
        this.setState({clickedDates: this.state.clickedDates.filter(function(d) { 
          return d !== fdate; 
        })});
      }
      else if (target.classList.contains('calendar-clickable')) {
        this.setState({
          clickedDates: this.state.clickedDates.concat([this.getDBDate(date)])
        }, () => target.classList.add('calendar-clicked'))
      }
    }
    else if (this.state.toggle === 'delete') {
      const date = new Date(target.dataset.date);
      const fdate = this.getDBDate(date);
      if (target.classList.contains('calendar-remove')) {
        target.classList.remove('calendar-remove');
        this.setState({clickedDates: this.state.clickedDates.filter(function(d) { 
          return d !== fdate; 
        })});
      }
      else if (target.classList.contains('calendar-clickable')) {
        this.setState({
          clickedDates: this.state.clickedDates.concat([this.getDBDate(date)])
        }, () => target.classList.add('calendar-remove'))
      }
    }
  }

  capitalise(s) {
    if (s !== null) {
      return s.charAt(0).toUpperCase() + s.slice(1)
    }
  }

  // On first mount
  componentDidMount() {
      this.generateMonth();
      this.isAuth();
  }

  render() {
    const numbers = this.state.days;
    const dates = numbers.map((n) =>
        <li key={n[0]} data-date={n[0]} className={n[1]} onClick={(e) => this.clickDate(e)}>{n[2]}</li>
    );

    const currentWeeks = this.state.currentWeeks;
    const weeks = currentWeeks.map((n) =>
        <li key={n} className="calendar-week-nr text-info noselect">{n}</li>
    );

    return (
      <>
        <div className={"calendar " + (this.props.user.toLowerCase() === 'anton' ? 'skewed ' : '')}>
            <div className="calendar-header mt-5">
                <button className="calendar-btn float-left" onClick={() => {this.prevMonth()}} ><FontAwesomeIcon icon={faArrowLeft} size="lg" className="ml-4 mr-4"/></button>
                <div className="calendar-date">{this.getFormattedDate(this.state.currentMonth)}</div>
                <button className="calendar-btn float-right" onClick={() => {this.nextMonth()}} ><FontAwesomeIcon icon={faArrowRight} size="lg" className="ml-4 mr-4"/></button>
            </div>
            <Row>
                <Col className="col-2">
                    <ul className="calendar-day-names">
                        <li className="calendar-week">
                            V.
                        </li>
                    </ul>
                    <ul className="calendar-days-list">
                        { weeks }
                    </ul>
                </Col>
                <Col className="col-10 calendar-body">
                    <ul className="calendar-day-names">
                    <li className="calendar-day">Mån</li>
                    <li className="calendar-day">Tis</li>
                    <li className="calendar-day">Ons</li>
                    <li className="calendar-day">Tors</li>
                    <li className="calendar-day">Fre</li>
                    <li className="calendar-day">Lör</li>
                    <li className="calendar-day">Sön</li>
                    </ul>
                    <ul className="calendar-days-list">
                        { dates }
                    </ul>
                </Col>
            </Row>
            {this.state.success && <p className="text-success text-left">{ this.state.success }</p>}
            {this.state.warning && <p className="text-danger text-left">{ this.state.warning }</p>}
            
            <Row className="text-left text-md-center">
              <Col className="col-8 offset-1 col-md-2 offset-md-3 mt-3">
                <span className="desc-today"></span> = Dagens datum
              </Col>
              <Col md className="col-8 offset-1 col-md-2 offset-md-0 mt-3">
                <span className="desc-booked"></span> = Bokat av annan
              </Col>
              <Col className="col-8 offset-1 col-md-2 offset-md-0 mt-3">
                <span className="desc-booked-self"></span> = Bokat av dig
              </Col>
            </Row>

            <Row className="mb-4">
              <Col className="col-10 offset-1 col-md-4 offset-md-2 mt-4">
                <Button variant="outline-dark" className="col-10 col-md-10 calendar-btn-toggle" onClick={() => {
                  this.state.toggle === 'standard' && this.enableBookingMode()
                  this.state.toggle === 'book' && this.setState({loading: true}, () => this.confirmBooking())
                  this.state.toggle === 'delete' && this.setState({loading: true}, () => this.confirmDeletion())
                  }} >
                    {this.state.toggle === 'standard' && 'Boka flera'}
                    {this.state.toggle === 'book' && 'Bekräfta bokning'}
                    {this.state.toggle === 'delete' && 'Bekräfta avbokning'}
                </Button>
              </Col>
              <Col md className="col-10 offset-1 col-md-4 offset-md-0 mt-4">
                <Button variant="outline-dark" className="col-10 col-md-10 calendar-btn-toggle" onClick={() => {
                  this.state.toggle === 'standard' && this.enableDeletionMode()
                  this.state.toggle === 'book' && this.disableBookingMode()
                  this.state.toggle === 'delete' && this.disableDeletionMode()
                  }} >
                    {this.state.toggle === 'standard' && 'Avboka flera'}
                    {this.state.toggle === 'book' && 'Avbryt bokning'}
                    {this.state.toggle === 'delete' && 'Avbryt avbokning'}
                  </Button>
              </Col>
            </Row>
          
            <Modal show={this.state.show} onHide={() => {this.setState({show: false, clickedDates: []}, () => this.generateDays())}} animation={false}>
              <Modal.Header closeButton>
                <Modal.Title>{this.state.clickedDates[0]}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {this.state.clickedDayUser ? 'Bokat av ' : 'Obokat'}
                {this.state.clickedDayUser && this.state.clickedDayUser.toLowerCase() === this.props.user ? 'dig' : this.state.clickedDayUser}
                <Row>
                {!this.state.clickedDayUser && (this.state.clickedDayOrig >= new Date(new Date().toDateString())) &&
                  <Button className="btn-success ml-3 mt-3" onClick={() => this.confirmBooking()}> 
                    Boka
                  </Button>
                }

                {this.state.clickedDayUser && (this.state.clickedDayOrig >= new Date(new Date().toDateString()) || this.state.isAuth) && (this.state.clickedDayUser.toLowerCase() === this.props.user || this.state.isAuth) && 
                  <Button className="btn-danger ml-3 mt-3" onClick={() => this.confirmDeletion()}> 
                    Avboka
                  </Button>
                }
                </Row>

              </Modal.Body>
            </Modal>
        </div>
      </>
    );
  }
}

export default Calendar;