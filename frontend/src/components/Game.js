import React, { Component } from 'react';
import { Image, Modal } from 'react-bootstrap';

export default class Game extends Component {
    
  constructor(props){
    super(props);
    this.state = {
        show: false
    }
  }

  startMove() {
    const rv = document.getElementById("rv");
    rv.style.top = Math.random()*100 + "%";
    rv.classList.add('rv')
    setTimeout(function() {
        this.stopMove();
    }.bind(this), 5000);
  }

  stopMove() {
    const rv = document.getElementById("rv");
    rv.classList.remove('rv');
    setTimeout(function() {
        this.startMove();
    }.bind(this), Math.random()*10*60*1000);
  }

  click(event) {
    var target = event.target;
    if (target.classList.contains('rv')) {
    this.setState({
        show: true
    })
    }
  }

  componentDidMount() {
      if (this.props.user) {
          setTimeout(
            function() {
                this.startMove();
            }
            .bind(this), Math.random()*1000);
      }
  }

	render() {
		return (
            <div>
                <Image id="rv" src={require('../assets/img/nav.png')} className="" onClick={(e) => this.click(e)}/>

                <Modal show={this.state.show} onHide={() => {this.setState({show: false})}} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Snyggt!</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Säg till mig så ska du få en vinst! /Herman
                    </Modal.Body>
                    </Modal>
            </div>
		);
	}
}